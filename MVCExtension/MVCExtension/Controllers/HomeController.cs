﻿using MVCExtension.Infrastructure;
using MVCExtension.Infrastructure.Filters;
using MVCExtension.Infrastructure.Model;
using MVCExtension.Infrastructure.ModelBinder;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MVCExtension.Controllers
{
    /// <summary>
    /// 测试
    /// </summary>
    public class HomeController : BaseController
    {
        public string Index()
        {
            return "hello world!";
        }

        #region 过滤器
        /*
         扩展阅读：
         * http://www.cnblogs.com/Showshare/p/exception-handle-with-mvcfilter.html
         * http://www.cnblogs.com/HopeGi/p/3342083.html
         */

        [TokenValidate]
        public JsonResult AuthorizeFilterTest()
        {
            return Json(new ReturnModel_Common { msg = "hello world!" });
        }

        //[LogException]
        public JsonResult HandleErrorFilterTest()
        {
            int i = int.Parse("abc");
            return Json(new ReturnModel_Data { data = i });
            //throw new Exception("throw Exception test...");
        }

        [SystemLog(Operate = "添加用户")]
        public string CustomerFilterTest()
        {
            Response.Write("<br/>Action 执行中...");
            return "<br/>Action 执行结束";
        }
        #endregion

        #region 模型绑定
        /*
         扩展阅读：
         * http://www.cnblogs.com/emrys5/p/asp-net-mvc-model-binder.html
         * http://www.cnblogs.com/miku/archive/2013/01/21/2866830.html
         * http://www.cnblogs.com/cjnmy36723/archive/2011/07/31/2122843.html
         * http://www.cnblogs.com/ldp615/archive/2010/07/30/sensitivewordsfiltermodelbinder.html
         */

        /// <summary>
        /// 模型绑定，测试：localhost:11597/Home/TokenBinderTest?token=1:汪杰:oppoic.cnblogs.com
        /// </summary>
        /// <param name="tokenModel"></param>
        /// <returns></returns>
        //public JsonResult TokenBinderTest([ModelBinder(typeof(TokenBinder))]TokenModel tokenModel)
        public JsonResult TokenBinderTest(TokenModel tokenModel)
        {
            var output = "Id：" + tokenModel.Id + "，Name：" + tokenModel.Name + "，Description：" + tokenModel.Description;
            return Json(new ReturnModel_Common { msg = output });
        }

        /// <summary>
        /// 去除实体里属性的首尾空格，测试：localhost:11597/Home/TrimBinderTest?Name= wangjie&Class= 三年级二班
        /// </summary>
        /// <param name="student"></param>
        /// <returns></returns>
        public JsonResult TrimBinderTest(Student student)
        {
            if (string.IsNullOrEmpty(student.Name) || string.IsNullOrEmpty(student.Class))
            {
                return Json(new ReturnModel_Common { msg = "未找到参数" });
            }
            else
            {
                return Json(new ReturnModel_Common { msg = "Name：" + student.Name + "，长度：" + student.Name.Length + " Class：" + student.Class + "，长度：" + student.Class.Length });
            }
        }
        #endregion

    }
}