﻿using MVCExtension.Infrastructure;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Web;
using System.Web.Mvc;

namespace MVCExtension.Controllers
{
    public class CommonController : Controller
    {
        /// <summary>
        /// 接口操作码自动生成，方便前端查看
        /// </summary>
        /// <returns></returns>
        public ContentResult GetReturnCode()
        {
            Dictionary<string, string> dic = StringAndDescriptionFromEnum(typeof(ReturnCode_Interface));
            StringBuilder sbStr = new StringBuilder();
            sbStr.Append("<!DOCTYPE html><html><head><title>接口返回码</title></head><body>");
            foreach (var item in dic.Keys)
            {
                sbStr.Append("<div style='font-weight:bold'>" + item + "</div>");
                foreach (var content in dic[item].Trim('\n').Split('\n'))
                {
                    sbStr.Append("<div>" + content + "</div>");
                }
                sbStr.Append("<br />");
            }
            sbStr.Append("</body></html>");
            return Content(sbStr.ToString());
        }

        /// <summary>
        /// 解析枚举
        /// </summary>
        /// <param name="EnumType"></param>
        /// <returns></returns>
        private static Dictionary<string, string> StringAndDescriptionFromEnum(Type EnumType)
        {
            FieldInfo[] fields = EnumType.GetFields(BindingFlags.Static | BindingFlags.Public);
            Dictionary<string, string> dic = new Dictionary<string, string>();
            foreach (var field in fields)
            {
                var dna = (DescriptionAttribute)Attribute.GetCustomAttribute(field, typeof(DescriptionAttribute));
                string key = string.Empty;
                if (dna == null || string.IsNullOrEmpty(dna.Description))
                {
                    key = "未知分类";
                }
                else
                {
                    key = dna.Description;
                }
                int value = (int)EnumType.InvokeMember(field.Name, BindingFlags.GetField, null, null, null);
                if (dic.ContainsKey(key))
                {
                    dic[key] += value + "：" + field.Name + "\n";
                }
                else
                {
                    dic.Add(key, value + "：" + field.Name + "\n");
                }
            }
            return dic;
        }

    }
}