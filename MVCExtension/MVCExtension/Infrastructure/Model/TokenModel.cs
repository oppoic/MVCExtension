﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MVCExtension.Infrastructure.Model
{
    /// <summary>
    /// Token（令牌）实体
    /// </summary>
    public class TokenModel
    {
        /// <summary>
        /// 主键
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// 姓名
        /// </summary>
        public string Name { set; get; }

        /// <summary>
        /// 简介
        /// </summary>
        public string Description { get; set; }

    }
}