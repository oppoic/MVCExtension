﻿using MVCExtension.Infrastructure.ModelBinder;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MVCExtension.Infrastructure.Model
{
    [ModelBinder(typeof(TrimModelBinder))]
    public class Student
    {
        public int Id { get; set; }

        [Trim]
        public string Name { get; set; }

        public string Class { get; set; }
    }
}