﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;

namespace MVCExtension.Infrastructure
{
    /// <summary>
    /// 接口返回类型_基本
    /// </summary>
    public class ReturnModel_Common
    {
        private bool _sucess = true;
        /// <summary>
        /// 是否成功
        /// </summary>
        public bool success { get { return _sucess; } set { _sucess = value; } }

        private ReturnCode_Interface _code = ReturnCode_Interface.操作成功;
        /// <summary>
        /// 操作码
        /// </summary>
        public ReturnCode_Interface code { get { return _code; } set { _code = value; } }

        private string _msg = "";
        /// <summary>
        /// 调试信息（此调试信息并不是提示用户的文字）
        /// </summary>
        public string msg { get { return _msg; } set { _msg = value; } }
    }

    /// <summary>
    /// 接口返回类型_简单数据
    /// </summary>
    public class ReturnModel_Data : ReturnModel_Common
    {
        /// <summary>
        /// 数据
        /// </summary>
        public dynamic data { get; set; }
    }


    /// <summary>
    /// 接口返回类型_BootstrapTable插件分页格式
    /// </summary>
    public class ReturnModel_BootstrapTable_Pager : ReturnModel_Common
    {
        /// <summary>
        /// 总数
        /// </summary>
        public int total { get; set; }

        /// <summary>
        /// 数据
        /// </summary>
        public dynamic rows { get; set; }
    }

    public enum ReturnCode_Interface
    {
        /*
         自动生成错误码对照表接口：/Common/GetReturnCode（注意：标注相同的Description可以在一起显示）
         */

        /*全局*/
        [Description("全局")]
        操作成功 = 0,
        [Description("全局")]
        操作失败 = -1,
        [Description("全局")]
        Token过期或错误 = -2,
        [Description("全局")]
        服务端抛错 = -10,

        /*用户*/
        [Description("用户")]
        用户名含非法字符 = 10001,


    }

}