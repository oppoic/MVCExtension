﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MVCExtension.Infrastructure
{
    /// <summary>
    /// 通用类
    /// </summary>
    public static class Common
    {
        /// <summary>
        /// 获取请求的GET或POST参数
        /// </summary>
        /// <returns></returns>
        public static string GetPostParas()
        {
            string param = string.Empty;
            if (HttpContext.Current.Request.HttpMethod.ToUpper() == "POST")
            {
                param += "[POST] ";
                for (int i = 0; i < HttpContext.Current.Request.Form.Count; i++)
                {
                    param += HttpContext.Current.Request.Form.Keys[i] + ":" + HttpContext.Current.Request.Form[i] + " ";
                }
            }
            else
            {
                param += "[GET] ";
                for (int i = 0; i < HttpContext.Current.Request.QueryString.Count; i++)
                {
                    param += HttpContext.Current.Request.QueryString.Keys[i] + ":" + HttpContext.Current.Request.QueryString[i] + " ";
                }
            }
            return param;
        }


    }
}