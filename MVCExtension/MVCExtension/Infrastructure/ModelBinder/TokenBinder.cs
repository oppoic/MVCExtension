﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MVCExtension.Infrastructure.ModelBinder
{
    /// <summary>
    /// Token解密
    /// </summary>
    public class TokenBinder : IModelBinder
    {
        public object BindModel(ControllerContext controllerContext, ModelBindingContext bindingContext)
        {
            var token = controllerContext.HttpContext.Request["token"];
            return BaseController.GetUserModelByToken(token);
        }
    }
}