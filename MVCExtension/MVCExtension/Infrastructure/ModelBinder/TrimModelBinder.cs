﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MVCExtension.Infrastructure.ModelBinder
{
    public class TrimModelBinder : DefaultModelBinder
    {
        protected override object GetPropertyValue(ControllerContext controllerContext, ModelBindingContext bindingContext, PropertyDescriptor propertyDescriptor, IModelBinder propertyBinder)
        {
            var obj = base.GetPropertyValue(controllerContext, bindingContext, propertyDescriptor, propertyBinder);
            if (obj is string && propertyDescriptor.Attributes[typeof(TrimAttribute)] != null)//判断是string类型且有[Trim]标记
            {
                return (obj as string).Trim();
            }
            return obj;
        }
    }
}