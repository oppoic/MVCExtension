﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MVCExtension.Infrastructure.ModelBinder
{
    [AttributeUsage(AttributeTargets.Property, AllowMultiple = false, Inherited = true)]
    public class TrimAttribute : Attribute
    {
    }
}