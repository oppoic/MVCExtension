﻿using NLog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MVCExtension.Infrastructure.Filters
{
    /// <summary>
    /// 异常过滤器（演示系统抛错时记录日志并返回特定格式的JSON）
    /// </summary>
    [AttributeUsage(AttributeTargets.Class, Inherited = true, AllowMultiple = false)]
    public class LogExceptionAttribute : HandleErrorAttribute
    {
        /// <summary>
        /// 处理未处理的异常
        /// </summary>
        /// <param name="filterContext"></param>
        public override void OnException(ExceptionContext filterContext)
        {
            if (!filterContext.ExceptionHandled)
            {
                string controllerName = (string)filterContext.RouteData.Values["controller"];
                string actionName = (string)filterContext.RouteData.Values["action"];
                string param = Common.GetPostParas();
                string ip = HttpContext.Current.Request.UserHostAddress;
                LogManager.GetLogger("LogExceptionAttribute").Error("Location：{0}/{1} Param：{2}UserIP：{3} Exception：{4}", controllerName, actionName, param, ip, filterContext.Exception.Message);

                filterContext.Result = new JsonResult
                {
                    Data = new ReturnModel_Common { success = false, code = ReturnCode_Interface.服务端抛错, msg = filterContext.Exception.Message },
                    JsonRequestBehavior = JsonRequestBehavior.AllowGet
                };
            }
            if (filterContext.Result is JsonResult)
                filterContext.ExceptionHandled = true;//返回结果是JsonResult，则设置异常已处理
            else
                base.OnException(filterContext);//执行基类HandleErrorAttribute的逻辑，转向错误页面
        }

    }
}