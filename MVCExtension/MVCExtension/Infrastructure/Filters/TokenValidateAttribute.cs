﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MVCExtension.Infrastructure.Filters
{
    /// <summary>
    /// 授权过滤器（演示验证接口token）
    /// </summary>
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Method, Inherited = true, AllowMultiple = false)]
    public class TokenValidateAttribute : AuthorizeAttribute
    {
        /// <summary>
        /// 授权验证的逻辑处理。返回true则通过授权，false则相反，然后进入下面的HandleUnauthorizedRequest方法
        /// </summary>
        /// <param name="httpContext"></param>
        /// <returns></returns>
        protected override bool AuthorizeCore(HttpContextBase httpContext)
        {
            string token = httpContext.Request["token"];
            if (string.IsNullOrEmpty(token))
            {
                return false;
            }
            var b = IsExpireToken(token);
            if (b)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// 授权失败处理
        /// </summary>
        /// <param name="filterContext"></param>
        protected override void HandleUnauthorizedRequest(AuthorizationContext filterContext)
        {
            base.HandleUnauthorizedRequest(filterContext);

            var json = new JsonResult();
            json.Data = new ReturnModel_Common
            {
                success = false,
                code = ReturnCode_Interface.Token过期或错误,
                msg = "token expired or error"
            };
            json.JsonRequestBehavior = JsonRequestBehavior.AllowGet;
            filterContext.Result = json;
        }

        private bool IsExpireToken(string token)
        {
            //从redis中获取token并验证

            return true;
        }

    }
}