﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MVCExtension.Infrastructure.Filters
{
    /// <summary>
    /// 自定义过滤器（演示记录系统操作日志）
    /// </summary>
    [AttributeUsage(AttributeTargets.Method, Inherited = true, AllowMultiple = false)]
    public class SystemLogAttribute : ActionFilterAttribute
    {
        public string Operate { get; set; }

        public override void OnActionExecuted(ActionExecutedContext filterContext)
        {
            filterContext.HttpContext.Response.Write("<br/>" + Operate + "：OnActionExecuted");
            base.OnActionExecuted(filterContext);
        }

        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            filterContext.HttpContext.Response.Write("<br/>" + Operate + "：OnActionExecuting");
            base.OnActionExecuting(filterContext);
        }

        public override void OnResultExecuted(ResultExecutedContext filterContext)
        {
            filterContext.HttpContext.Response.Write("<br/>" + Operate + "：OnResultExecuted");
            base.OnResultExecuted(filterContext);
        }

        public override void OnResultExecuting(ResultExecutingContext filterContext)
        {
            filterContext.HttpContext.Response.Write("<br/>" + Operate + "：OnResultExecuting");
            base.OnResultExecuting(filterContext);
        }

    }
}