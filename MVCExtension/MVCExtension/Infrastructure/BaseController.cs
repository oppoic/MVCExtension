﻿using MVCExtension.Infrastructure.ActionResultExt;
using MVCExtension.Infrastructure.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;

namespace MVCExtension.Infrastructure
{
    public class BaseController : Controller
    {

        protected override JsonResult Json(object data, string contentType, Encoding contentEncoding, JsonRequestBehavior behavior)
        {
            return new JsonNetResult
            {
                Data = data,
                ContentType = contentType,
                ContentEncoding = contentEncoding,
                JsonRequestBehavior = behavior
            };
        }

        /// <summary>
        /// 根据Token获取用户实体
        /// </summary>
        /// <param name="token"></param>
        /// <returns></returns>
        public static TokenModel GetUserModelByToken(string token)
        {
            if (!string.IsNullOrEmpty(token))
            {
                string[] array = token.Split(':');
                if (array.Length == 3)
                {
                    return new TokenModel() { Id = int.Parse(array[0]), Name = array[1], Description = array[2] };
                }
                else
                {
                    return new TokenModel() { Id = 0 };
                }
            }
            else
            {
                return new TokenModel() { Id = 0 };
            }
        }

    }
}